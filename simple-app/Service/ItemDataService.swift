//
//  ItemDataService.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import Foundation

class ItemDataService {
    
    static let instance = ItemDataService()
    
    private let items = [
        Item(name: "iPhone 8 plus 128GB Red Edition", price: "$999"),
        Item(name: "iPhone XR 128GB Green", price: "$799"),
        Item(name: "Pixel 3XL 128GB", price: "$899"),
        Item(name: "OnePlus 5T 128GB Black", price: "$699"),
        Item(name: "OnePlus 3T 64GB Black", price: "$399"),
        Item(name: "iPhone XS Max 256GB Gold", price: "$1099"),
        Item(name: "iPhone 7 128GB Gold", price: "$499"),
        Item(name: "OnePlus 6T 256GB Black", price: "$599")
    ]
    
    func getItems() -> [Item] {
        return items
    }
}
