//
//  ItemCell.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/24/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    func updateViews(item: Item) {
        name.text = item.name
        price.text = item.price
    }
}
