//
//  MusicPlayerVC.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import UIKit
import AVFoundation

class MusicPlayerVC: UIViewController {

    var player = AVAudioPlayer()
    let audioPath = Bundle.main.path(forResource: "my", ofType: "mp3")
    var timer = Timer()
    @IBOutlet weak var scrubber: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
            scrubber.maximumValue = Float(player.duration)
        } catch {
            //process error
        }
    }
    
    @objc func updateScrubber() {
        scrubber.value = Float(player.currentTime)
    }
    
    @IBAction func play(_ sender: Any) {
        player.play()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(MusicPlayerVC.updateScrubber), userInfo: nil, repeats: true)
    }
    
    @IBAction func scrubberMoved(_ sender: Any) {
        player.currentTime = TimeInterval(scrubber.value)
    }
    
    @IBAction func pause(_ sender: Any) {
        player.pause()
        timer.invalidate()
    }
    
    @IBAction func stop(_ sender: Any) {
        timer.invalidate()
        player.pause()
        scrubber.value = 0
        do {
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
            scrubber.maximumValue = Float(player.duration)
        } catch {
            //process error
        }
    }
    
}
