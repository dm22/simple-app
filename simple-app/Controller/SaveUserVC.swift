//
//  SaveUserVC.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import UIKit

class SaveUserVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var eMailTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveUser(_ sender: Any) {
        
        let usersOb = UserDefaults.standard.object(forKey: "users")
        var users: [String]
        let user = nameTxt.text!+" "+ageTxt.text!+" "+eMailTxt.text!
        
        if let tempUser = usersOb as? [String] {
            users = tempUser
            users.append(user)
        } else {
            users = [user]
        }
        
        UserDefaults.standard.set(users, forKey: "users")
        
        nameTxt.text = ""
        ageTxt.text = ""
        eMailTxt.text = ""
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
