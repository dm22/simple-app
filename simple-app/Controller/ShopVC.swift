//
//  ShopVC.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import UIKit
import UserNotifications

class ShopVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    @IBOutlet weak var itemsCollections: UICollectionView!
    
    private(set) public var items = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemsCollections.delegate = self
        itemsCollections.dataSource = self
        items = ItemDataService.instance.getItems()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [ .alert, .sound, .badge], completionHandler: {didAllow, error in})
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let content = UNMutableNotificationContent()
        content.title = "Added"
        content.body = "Item added to your cart."
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "timeDone", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as? ItemCell {
            let item = items[indexPath.row]
            cell.updateViews(item: item)
            return cell
        }
        return ItemCell()
    }
}
