//
//  Item.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import Foundation

struct Item {
    private(set) public var name: String
    private(set) public var price: String
    
    init(name: String, price: String) {
        self.name = name
        self.price = price
    }
}
