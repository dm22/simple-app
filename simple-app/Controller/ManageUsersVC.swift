//
//  ManageUsersVC.swift
//  simple-app
//
//  Created by Duleep-Madusanka on 12/22/18.
//  Copyright © 2018 Duleep-Madusanka. All rights reserved.
//

import UIKit

class ManageUsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var userTbl: UITableView!
    var users: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let usersOb = UserDefaults.standard.object(forKey: "users")
        if let tempUser = usersOb as? [String] {
            users = tempUser
        }
        
        userTbl.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "UserCell")
        cell.textLabel?.text = users[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            users.remove(at: indexPath.row)
            userTbl.reloadData()
            UserDefaults.standard.set(users, forKey: "users")
        }
    }
}
